# Tesseract Receipt Scanner

### Required Libraries

1. ImageMagic
2. Tesseract
2. Python 2.7+

### Installation

The following installation instructions will assume a linux enviroment.

1. Install tesseract:
```
sudo apt-get install tesseract-ocr
```
2. Install ImageMagic:
```
sudo apt-get install imagemagick
```
3. Python 2.7 should be default on most linux distributions.

### Receipt Processing Instructions

From the command line type:

```
python scanner.py"
```

This will:

1. Apply filtering to the input images (receipts) located at `/images/img` 
2. First filtering will process input images and place them at `/images/tmp`
3. Second filtering will process earlier pre-processed images and place them at `/images/pre-processed`
4. Tesseract will be executed on pre-processed images in previous step and place extracted text at `/images/ocr-text-pre`
5. Finally a spelling correcting algorithm will be executed on the extracted text and new text will be placed at `/images/ocr-text-post`

```
$ python scanner.py"

starting image pre-processing step 1 on img1.jpg
completed image pre-processing step 1 on img1.jpg
starting image pre-processing step 2 on img1.jpg
completed pre-processing step 2 on img1.jpg
starting text recognition on img1.jpg
Tesseract Open Source OCR Engine v3.03.00 with Leptonica
Warning in pixReadMemPng: work-around: writing to a temp file
completed text recognition on img1.jpg
starting text post-processing on img1.txt
completed text post-processing on img1.txt
starting image pre-processing step 1 on img10.jpg
completed image pre-processing step 1 on img10.jpg
starting image pre-processing step 2 on img10.jpg
completed pre-processing step 2 on img10.jpg
starting text recognition on img10.jpg
Tesseract Open Source OCR Engine v3.03.00 with Leptonica
Warning in pixReadMemPng: work-around: writing to a temp file
completed text recognition on img10.jpg
starting text post-processing on img10.txt
completed text post-processing on img10.txt
starting image pre-processing step 1 on img11.jpg
.
.
.
```

### Receipt Word Identification Statistics Instructions



From the command line type:

```
python statistics.py
```

This will:

1. Identify words in extracted text from dictionairies in the following categories: markets, sum, date, and phone number.
2. The data is saved in a CSV located at `data/results.csv`


```
$ python statistic.py

images/ocr-text-post/00-28-46/img1.txt aid None None
images/ocr-text-post/00-28-46/img10.txt kitchen 10/10/2012 None
images/ocr-text-post/00-28-46/img11.txt food 9/3/2005 123.40
images/ocr-text-post/00-28-46/img12.txt rite None None
images/ocr-text-post/00-28-46/img13.txt cafe None 6.42
images/ocr-text-post/00-28-46/img14.txt home depot 06/17/07 9.99
images/ocr-text-post/00-28-46/img15.txt burger 26/02/2009 7:51
images/ocr-text-post/00-28-46/img16.txt art None 105.00
images/ocr-text-post/00-28-46/img17.txt gas None None
images/ocr-text-post/00-28-46/img18.txt art None None
images/ocr-text-post/00-28-46/img19.txt cafe None 9.30
images/ocr-text-post/00-28-46/img2.txt target 04/26/2005 19.99
images/ocr-text-post/00-28-46/img20.txt cafe None 1.60
images/ocr-text-post/00-28-46/img21.txt restaurant 08/22/2014 None
images/ocr-text-post/00-28-46/img22.txt art 06/15/11 56.25
images/ocr-text-post/00-28-46/img23.txt cafe 04/10/2015 20.00
images/ocr-text-post/00-28-46/img24.txt music None 197.10
images/ocr-text-post/00-28-46/img25.txt food 01/25/12 44.07
images/ocr-text-post/00-28-46/img26.txt restaurant None 2:99
images/ocr-text-post/00-28-46/img27.txt rite aid 5/19/12 5/19
images/ocr-text-post/00-28-46/img28.txt tesco None 79.99
images/ocr-text-post/00-28-46/img29.txt food 13/03/13 0845 67
images/ocr-text-post/00-28-46/img3.txt kitchen 7/4/2013 11.50
images/ocr-text-post/00-28-46/img30.txt home None None
images/ocr-text-post/00-28-46/img31.txt fair None None
images/ocr-text-post/00-28-46/img32.txt art None None
images/ocr-text-post/00-28-46/img33.txt art None 65.99
images/ocr-text-post/00-28-46/img34.txt food None 2.99
images/ocr-text-post/00-28-46/img35.txt cafe None None
images/ocr-text-post/00-28-46/img36.txt cafe 7/09/10 1.20
images/ocr-text-post/00-28-46/img37.txt kitchen 2011/03/10 0002.00
images/ocr-text-post/00-28-46/img38.txt goodwill 10/08/2012 None
images/ocr-text-post/00-28-46/img39.txt tropical 6/17/2014 None
images/ocr-text-post/00-28-46/img4.txt cafe None None
images/ocr-text-post/00-28-46/img40.txt art None 0004 02
images/ocr-text-post/00-28-46/img5.txt cafe None 30.96
images/ocr-text-post/00-28-46/img6.txt cafe 88/87/2818 29.35
images/ocr-text-post/00-28-46/img7.txt music 01/07/2008 9.99
images/ocr-text-post/00-28-46/img8.txt restaurant None None
images/ocr-text-post/00-28-46/img9.txt supply 16/04/06 10.73
1449045674,40,40,19,5,26,

```

### Dictionary

The dictionary is located at `dictionary/big.txt` and more relavant words can be appended at the end.
