import re
from difflib import get_close_matches

class Receipt:
    def __init__(self, raw):
        self.market = self.date = self.sum = self.phone = None

        # Market name keys dictionary
        self.market_keys = [
            "cafe", "gas", "kitchen", "target", "station",
            "food", "books", "music", "movies", "movie", "home depot", "home",
            "giraffe", "art", "burger", "dennys", "denny",
            "restaurant", "italian", "supply", "rite aid", "rite", "aid", "groceries",
            "tesco", "trade fair", "trade", "fair", "pricesmart", "savon", "nofrills",
            "safeway", "goodwill", "disney", "tropical"
        ]

        # self.market_keys = [
        #     "cafe", "gas station", "supermarket", "restaurant"
        # ]

        # Sum name keys dictionary
        self.sum_keys = ["sum", "total", "total owed", "total paid", "total due", "cash payment",
                         "payment", "grand total", "cash tend", "amount due", "balance due",
                         "check total"]

        # self.sum_keys = ["sum", "total"]

        self.raw = map(str.lower, raw)
        self.raw = [line.decode('utf-8') for line in self.raw]
        self.parse()

    def parse(self):
        self.market = self.parse_market()
        self.date = self.parse_date()
        self.sum = self.parse_sum()
        self.phone = self.parse_phone()

    def fuzzy_find(self, keyword, accuracy=0.6):
        """
        Returns the first line in lines that contains a keyword.
        It runs a fuzzy match if 0 < accuracy < 1.0
        """
        for line in self.raw:
            words = line.split()
            # Get the single best match in line
            matches = get_close_matches(keyword, words, 1, accuracy)
            if matches:
                return line

    def parse_date(self):
        """
        Return the first date found in receipt text using simple regex
        """
        for line in self.raw:
            # e.g: 818-343-43489
            m = re.search(r'(\d+/\d+/\d+)', line)
            if m:
                # We're happy with the first match for now
                return m.group(1)

    def parse_phone(self):
        """
        Return the first phone number found in receipt text using simple regex
        """
        for line in self.raw:
            m = re.search(r'\d\d\d-\d\d\d-\d\d\d\d', line)
            if m:
                return m.group

    def parse_market(self):
        """
        Return the market found in receipt text using a predefined dictionary.
        If no match, then apply fuzzy_find
        """
        for line in self.raw:
            for market in self.market_keys:
                if market in line:
                    # found match from our list
                    return market
        # no direct match.
        # Need to do fuzzy matching
        for market in self.market_keys:
            line = self.fuzzy_find(market)
            if line:
                return market

    def parse_sum(self):
        """
        Return the sum found in receipt text using regex and fuzzy_find
        """
        for sum_key in self.sum_keys:
            sum_line = self.fuzzy_find(sum_key)
            if sum_line:
                # Replace all commas with a dot to make
                # finding and parsing the sum easier
                sum_line = sum_line.replace(',', '.')
                # Parse the sum
                sum_float = re.search('\d+(\.\s?|\,\s?|[^a-zA-Z\d])\d{2}', sum_line)
                if sum_float:
                    return sum_float.group(0)
