import subprocess
from spellcorrect import correct
from os import listdir, makedirs
from os.path import isfile, join, exists

def get_receipts(receipt_dir):
    """
    Checks if passed list of directories exists, otherwise creates
    them.
    :param receipt_dir:
    """
    try:
        receipt_images = [f for f in listdir(receipt_dir) if isfile(join(receipt_dir, f))]
        # Ignore hidden files like .DS_Store
        return [f for f in receipt_images if not f.startswith('.')]
    except Exception as e:
        print e
        print e.output

def exist_or_make_dir(dirs):
    """
    Checks if passed list of directories exists, otherwise creates
    them.
    :param dirs:
    """
    try:
        for d in dirs:
            if not exists(d):
                makedirs(d)
    except Exception as e:
        print e
        print e.output

def textcorrect(source, destination):
    """
    Iterate line by line in the source text file corrrecting words.
    These corrections are then written to a new file.

    Args:
        source: location of input text file
        destination: location of text file to save corrected text
    """
    try:
        with open(source) as infile, open(destination, 'w') as outfile:
            for line in infile:
                # split the line into a list of words
                words = line.split()

                # map lowercase method to list
                # TODO: only where one letter is uppercase
                words_lowercase = map(lambda x:x.lower(),words)

                # TODO: add relevant words to dictionary
                # map correct method to each lowercase word in list
                corrected_words = map(correct, words_lowercase)

                # join the list of words into a single string
                new_words_list = ' '.join(corrected_words)

                # write new corrcted words list to outfile with a newline
                outfile.write(new_words_list + "\n")
    except Exception as e:
        print e
        print e.output


def convert(source, destination):
    """
    Wrapper for convert library module from ImageMagic. This wrapper uses the following params:

    -auto-level [ automagically adjust color levels of image ]
    -colorspace gray [ alternate image colorspace ]
    -scale 200% [ scale the image ]
    -normalize [ transform image to span the full range of colors ]
    -colors 32 [ preferred number of colors in the image ]
    -sharpen 0x4.0 [ sharpen the image ]
    -contrast [ enhance or reduce the image contrast ]
    -trim [ trim image edges ]
    -background white [ background color ]

    Args:
        source: source path of image.
        destination: destination path of processed image.

    Returns:
    """

    try:
        # cmd = ['convert',
        #        '-auto-level',
        #        '-colorspace','gray',
        #        '-scale', '200%',
        #        '-normalize',
        #        '-colors', '32',
        #        '-sharpen', '0x4.0',
        #        '-contrast',
        #        '-trim',
        #        '-background','white',
        #        source,
        #        destination]

        # cmd = ['convert',
        #        '-scale', '170%',
        #        '-background' , 'white',
        #        '-density', '180',
        #        '-normalize',
        #        '-sharpen', '0x4.0',
        #        '-contrast',
        #        source,
        #        destination]

        cmd = ['convert',
               '-scale', '170%',
               '-background' , 'white',
               '-density', '600',
               '-normalize',
               '-sharpen', '0x4.0',
               '-contrast',
               source,
               destination]

        subprocess.check_call(cmd)
    except Exception as e:
        print(e)
        print(e.output)


def tesseract(source, destination):
    """
    Wrapper for tesseract OCR command line library.

    -l language

    Args:
        source: source path of image.
        destination: destination path of processed image.

    Returns:
    """

    try:
        cmd = ['tesseract',
               '-l', 'eng',
               source,
               destination]
        subprocess.check_call(cmd)
    except Exception as e:
        print(e)
        print(e.output)

def textcleaner(source, destination):
    """
    Wrapper for textcleaner shell script. Options used:

    -g convert document to grayscale before enhancing
    -e enhance image brightness before cleaning;choices are: none, stretch or normalize;
    -f size of filter used to clean background;integer>0; default=15
    -o offset of filter in percent used to reduce noise;integer>=0; default=5
    -s color saturation expressed as percent; integer>=0;only applicable if -g not set; a value of 100 is

    Args:
        source: source path of image.
        destination: destination path of processed image.

    Returns:
    """

    try:
        # cmd = ['./textcleaner.sh',
        #        '-g',
        #        '-e', 'normalize',
        #        '-f', '30',
        #        '-o', '12',
        #        '-s', '2',
        #        source,
        #        destination]

        cmd = ['./textcleaner.sh',
               '-g',
               '-e', 'stretch',
               '-f', '20',
               '-o', '10',
               '-s', '1',
               '-T',
               '-p', '10',
               source,
               destination]

        subprocess.call(cmd)
    except Exception as e:
        print(e)
        print(e.output)