from os import getcwd
from os.path import join
from time import strftime
from imutils import convert, tesseract, textcleaner, textcorrect, exist_or_make_dir, get_receipts

def main():
    """
    1. pre-processes receipts using ImageMagic convert module
    2. identifies text in receipts using tesseract OCR

    Note: JPEG is never good for images showing sharp lines, contrasts with uniform colors
          between different areas of the image, using only very few colors. This is true for
          black + white texts. JPEG is only "good" for typical photos, with lots of different
          colors and shading...therefore we will need to convert our images to PNG when using
          textcleaner.
    """

    # get time for each run to have unique location for run
    time = strftime("%d%m%y%H%M%S")

    receipt_dir = getcwd() + '/images/img'
    receipt_pre_dir = getcwd() + '/images/pre-processed/' + time
    receipt_text_pre_dir = getcwd() + '/images/ocr-text-pre/' + time
    receipt_text_post_dir = getcwd() + '/images/ocr-text-post/' + time
    receipt_tmp_dir = getcwd() + '/images/tmp/' + time

    exist_or_make_dir([receipt_pre_dir, receipt_text_pre_dir, receipt_text_post_dir, receipt_tmp_dir])

    receipts = get_receipts(receipt_dir)

    try:
        for receipt in receipts:
            print 'starting image pre-processing step 1 on %s' % receipt
            convert(join(receipt_dir, receipt), join(receipt_tmp_dir, receipt.split('.')[0] + '.png'))
            print 'completed image pre-processing step 1 on %s' % receipt

            print 'starting image pre-processing step 2 on %s' % receipt
            textcleaner(join(receipt_tmp_dir, receipt.split('.')[0] + '.png'), join(receipt_pre_dir, receipt.split('.')[0] + '.png'))
            print 'completed pre-processing step 2 on %s' % receipt

            print 'starting text recognition on %s' % receipt
            tesseract(join(receipt_pre_dir, receipt.split('.')[0] + '.png'), join(receipt_text_pre_dir, receipt.split('.')[0]))
            print 'completed text recognition on %s' % receipt

            print 'starting text post-processing on %s' % receipt.split('.')[0] + '.txt'
            textcorrect(join(receipt_text_pre_dir, receipt.split('.')[0] + '.txt'), join(receipt_text_post_dir, receipt.split('.')[0] + '.txt'))
            print 'completed text post-processing on %s' % receipt.split('.')[0] + '.txt'

    except Exception as e:
        print(e)
        print(e.output)


if __name__ == "__main__":
    main()
