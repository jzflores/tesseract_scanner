import time
from os import listdir
from os.path import isfile, join, isdir, getmtime
from collections import defaultdict
from receipt import Receipt

# Receipts should be simple text files containing extracted text
# Since creating a new directory for each run, need to find latest run directory

receipts_text_dir='images/ocr-text-post/'
all_subdirs = [join(receipts_text_dir, d) for d in listdir(receipts_text_dir)]
receipts_path = max(all_subdirs, key=getmtime)
results_csv_path = 'data/results.csv'


def main():
    receipt_files = [f for f in listdir(receipts_path) if isfile(join(receipts_path, f))]
    # Ignore hidden files like .DS_Store
    receipt_files = [f for f in receipt_files if not f.startswith('.')]
    stats = defaultdict(int)

    for receipt_file in receipt_files:
        receipt_path = join(receipts_path, receipt_file)
        with open(receipt_path) as receipt:
            lines = receipt.readlines()
            receipt = Receipt(lines)
            print receipt_path, receipt.market, receipt.date, receipt.sum
            stats["total"] += 1
            if receipt.market:
                stats["market"] += 1
            if receipt.date:
                stats["date"] += 1
            if receipt.sum:
                stats["sum"] += 1
            if receipt.phone:
                stats["phone"] += 1

    statistics(stats, True)


def statistics(stats, write=True):
    stats_str = "{0},{1},{2},{3},{4},{5},\n".format(
        int(time.time()), stats["total"], stats["market"], stats["date"], stats["phone"], stats["sum"])
    print stats_str
    if write:
        with open(results_csv_path, "a") as stats_file:
            stats_file.write(stats_str)


def percent(nom, denom):
    return str(int(nom / float(denom) * 100)) + "%"


if __name__ == "__main__":
    main()
